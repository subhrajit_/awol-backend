from flask import Flask, request, jsonify
import json
import os 
from gevent.pywsgi import WSGIServer
from iterapi import iterapi

app = Flask(__name__)

@app.route("/attendance", methods=['GET','POST'])
def attend():
	if request.method == 'GET':
			return "403"
	elif request.method == 'POST':
		if request.is_json:
			user = request.get_json()['user']
			passwd = request.get_json()['pass']
		else:
			user = request.form['user']
			passwd = request.form['pass']

		st = iterapi.Student(user,passwd)
		st.getAttendance()
		if st.attendance:
			return jsonify(st.attendance)
		else:
			return "404"
		
@app.route("/studentinfo", methods=['GET','POST'])
def info():
	if request.method == 'GET':
			return "403"
	elif request.method == 'POST':
		if request.is_json:
			user = request.get_json()['user']
			passwd = request.get_json()['pass']
		else:
			user = request.form['user']
			passwd = request.form['pass']

		st = iterapi.Student(user,passwd)
		st.getInfo()
		if st.details:
			return jsonify(st.details)
		else:
			return "404"


if __name__ == '__main__':
	port = int(os.environ.get('PORT', 5000))
	http_server = WSGIServer(('',port),app)
	http_server.serve_forever()